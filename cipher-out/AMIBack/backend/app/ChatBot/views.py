from django.shortcuts import render

from rest_framework import exceptions, permissions, status, viewsets
from rest_framework.response import Response
from backend.app.ChatBot.models import Chat, Client, Message

from backend.app.ChatBot.serializers import ChatSerializer, MessageCreateSerializer, MessageSerializer


from rest_framework.decorators import api_view


class ChatViewSet(viewsets.ModelViewSet):
    """ For Chat conv with bot """
    serializer_class = ChatSerializer

    def get_permissions(self):
        if self.action in ["list", "retrieve"]:
            permission_classes = [permissions.AllowAny]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]

    def get_queryset(self):
        user = self.request.user
        return Chat.objects.filter(owner__pk=user.pk)


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    def get_permissions(self):
        if self.action in ["list", "retrieve"]:
            permission_classes = [permissions.AllowAny]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]

    def get_serializer_class(self):
        if self.action == "create":
            return MessageCreateSerializer
        else:
            return MessageSerializer


@api_view(['POST'])
def sendMessage(request):
    # print("request ...", request)
    # _body = request.data.get('body', '')
    # try:
    #     client = Client.objects.get(pk=request.user.id)
    #     chat = Chat.objects.filter(owner=request.user)
    #     # if we gat client
    #     # create new message
    #     new_msg = Message(body=_body)
    #     new_msg.save()
    #     if new_msg.owner == client.user:
    #         chat.messages.add(new_msg)
    #         return Response("sent", status=status.HTTP_201_CREATED)
    #     else:
    #         return Response('error', status=status.HTTP_400_BAD_REQUEST)

    # except Client.DoesNotExist:
    #     return Response('Client notFound', status.HTTP_404_NOT_FOUND)
    # except Chat.DoesNotExist:
    #     return Response('Chat notFound', status.HTTP_404_NOT_FOUND)
    user = request.user
    msg = request.data
    

