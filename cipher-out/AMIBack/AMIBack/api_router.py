
from django.conf import settings
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import DefaultRouter, SimpleRouter
from rest_framework_simplejwt.views import (TokenObtainPairView,
                                            TokenRefreshView)

# from backend.compta.views import OrganizationViewSet
if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

# router.register("users", UserAPIViewSet)
# router.register("organizations", OrganizationViewSet)

app_name = "api"
urlpatterns = router.urls

schema_view = get_schema_view(
    openapi.Info(
        title="AMI assurances API",
        default_version='v1',
        description="AMI assurances API documentation"
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)
from rest_auth.registration.views import VerifyEmailView, RegisterView

urlpatterns += [
    # rest auth
    path("", include("backend.app.ChatBot.urls")),
    path("", include("backend.app.FAQ.urls")),
    path("auth/", include("dj_rest_auth.urls")),
    # path('auth/registration/', RegisterView.as_view(), name='account_signup'),
    path("auth/registration/", include("dj_rest_auth.registration.urls")),
    path('jwt/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('jwt/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    
    # swagger api documentation
    re_path(r'^swagger(?P<format>)$', schema_view.without_ui(
        cache_timeout=0), name='schema-json'),
    path("swagger/", schema_view.with_ui('swagger',
         cache_timeout=0), name='schema-swagger-ui'),

]

